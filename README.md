# ReMoVES FE data

Data related to the ReMoVES exergame practice by a population of frail elders.
If you are using this dataset in your work, please acknowledge the source (Department of Electrical, Electronics and Telecommunication Engineering and Naval Architecture (DITEN), Università degli Studi di Genova) and reference this paper:

Trombini, M.; Ferraro, F.; Morando, M.; Regesta, G.; Dellepiane, S. A Solution for the Remote Care of Frail Elderly Individuals via Exergames. Sensors 2021, 21, 2719. https://doi.org/10.3390/s21082719

https://www.mdpi.com/1424-8220/21/8/2719/htm
